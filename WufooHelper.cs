using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;

namespace Wufoo
{
    public class WufooHelper
    {

        protected static string GetBaseUrl(string wufooSubDomain)
        {
            return String.Format("https://{0}.wufoo.com/api/v3/",wufooSubDomain);
        }

        protected static string SendApiRequest(string wufooSubDomain, string wufooApiKey, string url)
        {
            var request = (HttpWebRequest)HttpWebRequest.Create(GetBaseUrl(wufooSubDomain) + url);

            var authInfo = wufooApiKey + ":" + "footastic";
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;

            using (var webResponse = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(webResponse.GetResponseStream()))
                {
                    return reader.ReadToEnd();
                }

            }
        }

        public static WufooFormEntry GetFormEntry(string wufooSubDomain, string wufooApiKey, string formId, string entryId)
        {
            try
            {
                if (formId == null || entryId == null)
                {
                    return null;
                }

                var url = String.Format("forms/{0}/entries.json?Filter1=EntryID+Is_equal_to+{1}", formId, entryId);
                var jsonResponse = SendApiRequest(wufooSubDomain, wufooApiKey, url);

                return new WufooFormEntry(JObject.Parse(jsonResponse), formId);
            }
            catch (Exception)
            {
                //Add error log entry
                return null;
            }
        }
    }


}