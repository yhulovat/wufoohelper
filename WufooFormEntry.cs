using System;
using Newtonsoft.Json.Linq;

namespace Wufoo
{
    public class WufooFormEntry
    {
        private JToken _data;
        private string _formId;

        public WufooFormEntry(JToken jtoken, string formId)
        {
            this._data = jtoken;
            this._formId = formId;
        }

        public WufooFormEntry(JObject jobject, string formId): this(jobject["Entries"][0],formId)
        {}

        public string GetFieldValue(string fieldName)
        {
            try
            {
                return (string)this._data[fieldName];
            }
            catch (Exception ex)
            {
                //add error log entry
                return null;
            }
        }

        public string GetFieldValue(int fieldId)
        {
            return this.GetFieldValue("Field"+fieldId);
        }


        public decimal? GetFieldDecimalValueByConfigMapping(int fieldId)
        {
            try
            {
                var value = GetFieldValue(fieldId);
                return value == null ? null : Convert.ToDecimal(value);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public string GetId()
        {
            return GetFieldValue("EntryId");
        }

        public string GetDateCreated()
        {
            return GetFieldValue("DateCreated");
        }
    }
}